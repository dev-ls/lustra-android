package com.lustra.lustra.Helpers;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class PermissionHelper {

    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] CAMERA_PERMS={
            Manifest.permission.CAMERA
    };
    private static final String[] CONTACTS_PERMS={
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private Context mContext;

    public PermissionHelper(Context context){
        mContext = context;
    }


    public boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    public boolean canAccessCamera() {
        return(hasPermission(Manifest.permission.CAMERA));
    }

    public boolean canAccessContacts() {
        return(hasPermission(Manifest.permission.READ_CONTACTS));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==mContext.checkSelfPermission(perm));
    }

    public void requestLocationPermissions(Activity activity){
        ActivityCompat.requestPermissions(activity,LOCATION_PERMS,100);
    }

}
