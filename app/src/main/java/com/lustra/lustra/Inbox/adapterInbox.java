package com.lustra.lustra.Inbox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.dateHelper;
import com.lustra.lustra.R;

import java.util.List;

/**
 * Created by victorrosas on 3/17/18.
 */

public class adapterInbox extends ArrayAdapter<OBJ_Inboxmsg> {

    public adapterInbox(Context context, List<OBJ_Inboxmsg> objects){
        super(context,0,objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Salvando la referencia del View de la fila
        View listItemView = convertView;

        //Comprobando si el View no existe
        if (null == convertView) {
            //Si no existe, entonces inflarlo con image_list_view.xml
            listItemView = inflater.inflate(
                    R.layout.cell_inbox_msg,
                    parent,
                    false);

        }

        ImageView imgMsgPerson = listItemView.findViewById(R.id.img_message_person);
        TextView txtPersonName = listItemView.findViewById(R.id.inbox_txt_name);
        TextView txtReceivedAgo = listItemView.findViewById(R.id.inbox_txt_received_ago);
        TextView txtShortMessage = listItemView.findViewById(R.id.inbox_txt_short_message);
        TextView txtStatus = listItemView.findViewById(R.id.inbox_txt_status);
        TextView txtDateReceived = listItemView.findViewById(R.id.inbox_txt_received);

        OBJ_Inboxmsg message = getItem(position);

        imgMsgPerson.setImageBitmap(message.getImgProfile());
        txtPersonName.setText(message.getName());
        txtReceivedAgo.setText("");
        txtShortMessage.setText(message.getMessageShort());
        txtStatus.setText(message.getStatus());
        txtDateReceived.setText(dateHelper.DateTimetoString(message.getReceived()));

        return listItemView;
    }


}
