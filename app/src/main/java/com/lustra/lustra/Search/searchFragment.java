package com.lustra.lustra.Search;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lustra.lustra.Helpers.Objects.OBJ_Employee;
import com.lustra.lustra.Helpers.Objects.OBJ_Review;
import com.lustra.lustra.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by victorrosas on 3/13/18.
 */

public class searchFragment extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    private GoogleMap googleMap;
    ArrayList<OBJ_Location> locations;
    OBJ_Location selected_Location;
    ListView lvEmployees;
    adapterEmployees laEmployees;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);


        mapView = v.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        setup(v);

        return v;
    }

    public void setup(View v) {

        try {
            MapsInitializer.initialize(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ///Get employees
        lvEmployees = v.findViewById(R.id.lv_employees);
        laEmployees = new adapterEmployees(getContext(), get_employees());
        lvEmployees.setAdapter(laEmployees);

        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        locations = get_Locations();
        for (int i = 0; i < locations.size(); i++) {
            System.out.print(i);
            System.out.print(locations.size());
            OBJ_Location location = locations.get(i);
            this.googleMap.addMarker(new MarkerOptions().position(location.location_LatLng).title(location.title));

            if (i >= locations.size() - 1) {
                float zoomLevel = (float) 15.00;
                this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location.location_LatLng, zoomLevel));
                //set_LocationInfo(locations.get(i));
                selected_Location = locations.get(i);
            }
        }

        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                System.out.println(marker.getTitle());
                for (int i = 0; i < locations.size(); i++) {
                    if (locations.get(i).title.equalsIgnoreCase(marker.getTitle())) {
                        //set_LocationInfo(locations.get(i));
                        selected_Location = locations.get(i);
                    }
                }
                return false;
            }
        });

    }

    public ArrayList<OBJ_Location> get_Locations() {

        //Traemos las locations con WS

        /*Location locationCt = null;
        LocationManager locationManagerCt = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionHelper permissionHelper = new PermissionHelper(getContext());
            permissionHelper.canAccessLocation();
            return null;
        }
        locationCt = locationManagerCt.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        LatLng latLng = new LatLng(locationCt.getLatitude(), locationCt.getLongitude());
        System.out.println(latLng);*/

        locations = new ArrayList<>();
        //locations.add(new OBJ_Location("100",1,"Title 1",latLng));
        locations.add(new OBJ_Location("100",1,"Title 1",new LatLng(28.673968, -106.079479)));
        locations.add(new OBJ_Location("200",2,"Title 2",new LatLng(28.672434, -106.101619)));
        return locations;
    }

    public ArrayList<OBJ_Employee> get_employees(){
        ArrayList<OBJ_Employee> employees = new ArrayList<>();

        Map<Integer,DateTime[]> workinghours = new HashMap<>();
        DateTime time = new DateTime();
        DateTime[] times = {time};
        workinghours.put(1,times);
        workinghours.put(2,times);
        workinghours.put(3,times);
        workinghours.put(4,times);
        workinghours.put(5,times);
        workinghours.put(6,times);
        workinghours.put(0,times);

        Bitmap profilepic =  ((BitmapDrawable) ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.person_2_profile_, null)).getBitmap();
        OBJ_Review[] ratings = {new OBJ_Review(1,profilepic,"Name",1,"Test Review")};

        String tags[] = {"Jardineria","Jardin"};

        employees.add(new OBJ_Employee(1, profilepic,"Jorge","--------",workinghours,ratings,300.00,"Paisajismo",tags));
        employees.add(new OBJ_Employee(2, profilepic,"Paul","-------",workinghours,ratings,150.00,"Lavado de carros",tags));
        employees.add(new OBJ_Employee(3, profilepic,"Miriam","1+ años",workinghours,ratings,500.00,"Diseño de exteriores",tags));
        employees.add(new OBJ_Employee(4, profilepic,"Don Jose","20+ años",workinghours,ratings,650.00,"Jardineria general",tags));

        return employees;
    }


}
