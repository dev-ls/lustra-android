package com.lustra.lustra.Settings.paymentMethods;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.ListView;

import com.lustra.lustra.Helpers.Objects.OBJ_Card;
import com.lustra.lustra.R;

import java.util.ArrayList;
import java.util.List;

public class paymentMethodFragment extends FragmentActivity {

    ListView lvPaymentMethods;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment_methods);
        setup();
    }

    public void setup(){
        lvPaymentMethods = (ListView) findViewById(R.id.lv_cards);
        adapterCard adapterCards = new adapterCard(this,getCards());
    }

    public List<OBJ_Card> getCards(){
        List<OBJ_Card> cards = new ArrayList<OBJ_Card>();
        cards.add(new OBJ_Card("0","4242","Principal",true));

        return cards;
    }


}
