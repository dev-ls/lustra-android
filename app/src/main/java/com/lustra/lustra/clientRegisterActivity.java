package com.lustra.lustra;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.lustra.lustra.Helpers.AlertHelper;
import com.lustra.lustra.Helpers.SessionHelper;

/**
 * Created by victorrosas on 2/27/18.
 */

public class clientRegisterActivity extends Activity {

    private EditText txtName;
    private EditText txtLastName;
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText txtPhone;

    private Button btnRegister;

    private FirebaseAuth mAuth;
    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_register);
        setup();
    }

    public void setup(){

        mContext = this;

        txtName = findViewById(R.id.register_txt_name);
        txtLastName = findViewById(R.id.register_txt_lastname);
        txtEmail = findViewById(R.id.register_txt_email);
        txtPassword = findViewById(R.id.register_txt_password);
        txtPhone = findViewById(R.id.register_txt_phone);

        btnRegister = findViewById(R.id.register_btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = txtName.getText().toString();
                String lastname = txtLastName.getText().toString();
                String email = txtEmail.getText().toString();
                String password = txtPassword.getText().toString();
                String phone = txtPhone.getText().toString();

                register(name,lastname,email,password,phone);
            }
        });
    }

    public void register(String name,String lastname,String email,String password,String phone){

        final String fullname = name + " " + lastname;

        mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(fullname)
                                    .build();

                            user.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                AlertHelper.create_SimpleAlert(mContext,"Exito","Registro correcto");
                                                SessionHelper.login(mContext);
                                            }
                                        }
                                    });


                        } else {
                            // If sign in fails, display a message to the user.
                            AlertHelper.createSimpleToast(mContext,"Failed Login");
                        }


                    }
                });

    }

}
