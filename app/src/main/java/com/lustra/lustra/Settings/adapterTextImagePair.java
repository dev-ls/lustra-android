package com.lustra.lustra.Settings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.R;

import java.util.List;

/**
 * Created by victorrosas on 3/17/18.
 */

public class adapterTextImagePair extends ArrayAdapter<OBJ_TextImagePair> {

    public adapterTextImagePair(Context context, List<OBJ_TextImagePair> objects){
        super(context,0,objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Salvando la referencia del View de la fila
        View listItemView = convertView;

        //Comprobando si el View no existe
        if (null == convertView) {
            //Si no existe, entonces inflarlo con image_list_view.xml
            listItemView = inflater.inflate(
                    R.layout.cell_settings,
                    parent,
                    false);

        }

        TextView txtSetting = listItemView.findViewById(R.id.settings_txt_name);
        ImageView imgSettingIcon = listItemView.findViewById(R.id.settings_img_icon);

        OBJ_TextImagePair item = getItem(position);

        txtSetting.setText(item.getTextValue());
        imgSettingIcon.setImageDrawable(item.getImg());


        return listItemView;
    }

}
