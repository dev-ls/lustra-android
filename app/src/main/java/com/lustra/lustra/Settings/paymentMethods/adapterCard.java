package com.lustra.lustra.Settings.paymentMethods;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.Objects.OBJ_Card;
import com.lustra.lustra.R;

import java.util.List;

public class adapterCard extends ArrayAdapter<OBJ_Card> {

    public adapterCard(Context context, List<OBJ_Card> objects){
        super(context,0,objects);
    }

    public View getView(int position, View convertView, ViewGroup parent){
        //Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Salvando la referencia del View de la fila
        View listItemView = convertView;

        //Comprobando si el View no existe
        if (null == convertView) {
            //Si no existe, entonces inflarlo con image_list_view.xml
            listItemView = inflater.inflate(
                    R.layout.list_new_card,
                    parent,
                    false);

        }

        ImageView isDefault = (ImageView) listItemView.findViewById(R.id.imgview_isdefault);
        TextView last4 = (TextView) listItemView.findViewById(R.id.txt_last4);

        OBJ_Card item = getItem(position);

        last4.setText(item.card_last4);

        if(item.card_isDefault){
            isDefault.setImageResource(android.R.drawable.btn_star_big_on);
        }

        return listItemView;
    }

}
