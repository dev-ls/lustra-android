package com.lustra.lustra.Profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.UserPicture;
import com.lustra.lustra.Helpers.Objects.OBJ_Review;
import com.lustra.lustra.R;

import java.util.List;

/**
 * Created by victorrosas on 3/19/18.
 */

public class adapterReviews extends ArrayAdapter<OBJ_Review> {

    public adapterReviews(Context context, List<OBJ_Review> objects){
        super(context,0,objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Salvando la referencia del View de la fila
        View listItemView = convertView;

        //Comprobando si el View no existe
        if (null == convertView) {
            //Si no existe, entonces inflarlo con image_list_view.xml
            listItemView = inflater.inflate(
                    R.layout.cell_reviews,
                    parent,
                    false);

        }

        ImageView imgReviewerProfile = listItemView.findViewById(R.id.review_img_reviewer);
        TextView txtReviewerName = listItemView.findViewById(R.id.review_txt_reviewer_name);
        TextView txtReview = listItemView.findViewById(R.id.review_txt_review);
        ImageView imgStars = listItemView.findViewById(R.id.review_img_star_rating);

        OBJ_Review item = getItem(position);

        imgReviewerProfile.setImageBitmap(UserPicture.getRoundedShape(item.getReviewerImage()));
        txtReviewerName.setText(item.getReviewerName());
        txtReview.setText(item.getReviewString());
        imgStars.setImageDrawable(getContext().getDrawable(R.drawable.starts));

        return listItemView;
    }

}
