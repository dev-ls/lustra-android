package com.lustra.lustra.Helpers.Objects;

import android.graphics.drawable.Drawable;

/**
 * Created by victorrosas on 4/17/18.
 */

public class OBJ_Service {

    String serviceCategory;
    String serviceDescription;
    Drawable serviceImage;

    public OBJ_Service(String serviceCategory, String serviceDescription, Drawable serviceImage) {
        this.serviceCategory = serviceCategory;
        this.serviceDescription = serviceDescription;
        this.serviceImage = serviceImage;
    }

    public String getServiceCategory() {
        if(serviceCategory != null) {
            return serviceCategory;
        }else {
            return "";
        }
    }

    public String getServiceDescription() {
        if(serviceDescription != null) {
            return serviceDescription;
        }else{
            return "";
        }
    }

    public Drawable getServiceImage() {
        return serviceImage;
    }
}
