package com.lustra.lustra.Calendar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.lustra.lustra.Helpers.dateHelper;
import com.lustra.lustra.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by victorrosas on 4/3/18.
 */

public class calendarAgendaFragment extends Fragment {

    CalendarView calendarView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calendar_appointment,container,false);
        setup(v);
        return v;
    }

    public void setup(View v){
        calendarView = v.findViewById(R.id.calendarView);
        calendarView.setEvents(getEvents());

        try {
            calendarView.setDate(Calendar.getInstance());
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }

    }

    public List<EventDay> getEvents(){
        List<EventDay> events = new ArrayList<>();

        DateTime dt = new DateTime(2018,4,4,1,0,0,0);
        DateTime dt2 = new DateTime(2018,4,5,1,0,0,0);
        DateTime dt3 = new DateTime(2018,4,6,1,0,0,0);
        DateTime dt4 = new DateTime(2018,4,12,1,0,0,0);
        DateTime dt5 = new DateTime(2018,4,14,1,0,0,0);

        Calendar calendar = Calendar.getInstance();
        Calendar date2 = dateHelper.dateToCalendar(dt.toDate());
        Calendar date3 = dateHelper.dateToCalendar(dt2.toDate());
        Calendar date4 = dateHelper.dateToCalendar(dt3.toDate());
        Calendar date5 = dateHelper.dateToCalendar(dt4.toDate());
        Calendar date6 = dateHelper.dateToCalendar(dt5.toDate());

        events.add(new EventDay(calendar, R.drawable.person_2_profile_));
        events.add(new EventDay(date2, R.drawable.person_2_profile_));
        events.add(new EventDay(date3, R.drawable.person_2_profile_));
        events.add(new EventDay(date4, R.drawable.person_2_profile_));
        events.add(new EventDay(date5, R.drawable.person_2_profile_));
        events.add(new EventDay(date6, R.drawable.person_2_profile_));

        return events;
    }

}
