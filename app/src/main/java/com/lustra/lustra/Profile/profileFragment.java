package com.lustra.lustra.Profile;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.Objects.OBJ_Review;
import com.lustra.lustra.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victorrosas on 3/19/18.
 */

public class profileFragment extends FragmentActivity {

    ImageView imgProfile;
    TextView txtName;
    TextView txtLocation;
    ImageView imgStars;
    TextView txtReviewCount;
    ImageView imgIsVerified;
    TextView txtDescription;
    TextView txtMemberSince;
    TextView txtExperience;
    TextView txtReviewNumber;
    ListView lvReviews;
    adapterReviews laReviews;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        setup();
    }

    public void setup(){

        imgProfile = findViewById(R.id.profile_img_profile);
        imgProfile.setImageDrawable(getDrawable(R.drawable.person_2_profile_));
        txtName = findViewById(R.id.profile_txt_name);
        txtName.setText("Name");
        txtLocation = findViewById(R.id.profile_txt_location);
        txtLocation.setText("Location");
        imgStars = findViewById(R.id.profile_img_stars);
        imgStars.setImageDrawable(getDrawable(R.drawable.starts));
        txtReviewCount = findViewById(R.id.profile_txt_reviewcount);
        txtReviewCount.setText("Review Count");
        imgIsVerified = findViewById(R.id.profile_img_isverified);
        imgIsVerified.setVisibility(View.VISIBLE);
        txtDescription = findViewById(R.id.profile_txt_description);
        txtDescription.setText("Description");
        txtMemberSince = findViewById(R.id.profile_txt_membersince);
        txtMemberSince.setText("Member Since");
        txtExperience = findViewById(R.id.profile_txt_experience);
        txtExperience.setText("Experience");
        txtReviewNumber = findViewById(R.id.profile_txt_reviewnum);
        txtReviewNumber.setText("Number of Reviews");
        lvReviews = findViewById(R.id.profile_lv_reviews);
        laReviews = new adapterReviews(this,getReviews());
        lvReviews.setAdapter(laReviews);
    }

    public List<OBJ_Review> getReviews(){
        List<OBJ_Review> reviews = new ArrayList<>();
        Bitmap profilepic =  ((BitmapDrawable) ResourcesCompat.getDrawable(this.getResources(), R.drawable.person_2_profile_, null)).getBitmap();
        reviews.add(new OBJ_Review(1,profilepic,"Name",1,"Test Review"));
        reviews.add(new OBJ_Review(1,profilepic,"Name",1,"Test Review"));
        reviews.add(new OBJ_Review(1,profilepic,"Name",1,"Test Review"));

        return reviews;
    }

}
