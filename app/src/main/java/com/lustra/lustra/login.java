package com.lustra.lustra;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lustra.lustra.Helpers.AlertHelper;
import com.lustra.lustra.Helpers.SessionHelper;

public class login extends AppCompatActivity {

    private EditText txtUser;
    private EditText txtPassword;
    private Button btnLogin;
    private FirebaseAuth mAuth;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setup();
    }

    public void setup() {

        mContext = this;

        txtUser = findViewById(R.id.login_txt_user);
        txtPassword = findViewById(R.id.login_txt_password);

        btnLogin = findViewById(R.id.login_btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn(txtUser.getText().toString(),txtPassword.getText().toString());

            }
        });

    }

    public void signIn(String email,String password){
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            SessionHelper.login(mContext);
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.

                            AlertHelper.createSimpleToast(mContext,"Contraseña incorrecta o usuario no existe");
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    /*@Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }*/

}
