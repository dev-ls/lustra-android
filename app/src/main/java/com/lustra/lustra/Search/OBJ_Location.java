package com.lustra.lustra.Search;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by victorrosas on 3/14/18.
 */

public class OBJ_Location {

    String price;
    Integer id;
    String title;
    public LatLng location_LatLng;

    public OBJ_Location(String price, Integer id, String title, LatLng location_LatLng) {
        this.price = price;
        this.id = id;
        this.title = title;
        this.location_LatLng = location_LatLng;
    }
}
