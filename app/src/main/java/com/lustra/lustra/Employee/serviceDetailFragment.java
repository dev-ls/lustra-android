package com.lustra.lustra.Employee;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.res.ResourcesCompat;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.UserPicture;
import com.lustra.lustra.R;

/**
 * Created by victorrosas on 4/2/18.
 */

public class serviceDetailFragment extends FragmentActivity {

    ImageView imgService;
    ImageView imgService2;
    TextView txtTitle;
    TextView txtSubtitle;
    TextView txtEmployeeCity;
    ImageView imgEmployee;
    TextView txtServiceDetailDescription;
    TextView txtPrice;
    Button btnHire;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_employee_service_description);
        setup();
    }

    public void setup(){

        Bitmap profilepic =  ((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.person_2_profile_, null)).getBitmap();
        Double price = getIntent().getDoubleExtra("tagPrice",0.0);

        imgService = findViewById(R.id.service_img_service);
        imgService.setImageDrawable(getDrawable(R.drawable.gardening_));
        imgService2 = findViewById(R.id.service_img_service2);
        imgService2.setImageDrawable(getDrawable(R.drawable.gardening_landscaping));
        txtTitle = findViewById(R.id.service_detail_txt_title);
        txtTitle.setText("Servicios Jardineria");
        txtSubtitle = findViewById(R.id.service_detail_txt_subtitle);
        txtSubtitle.setText("Especializado en paisajismo,sistemas de riego y podados de cesped");
        txtEmployeeCity = findViewById(R.id.service_detail_employee_txt_location);
        txtEmployeeCity.setText("Ciudad Juarez,Chihuahua,MX");
        imgEmployee = findViewById(R.id.service_detail_employee_img_profile);
        imgEmployee.setImageBitmap(UserPicture.getRoundedShape(profilepic));
        txtServiceDetailDescription = findViewById(R.id.service_detail_employee_txt_description);
        txtServiceDetailDescription.setText("El servicio incluye una de las tres opciones,pero puedo expandir a mas,si te interesa el servicio lo minimo que puedo hacer son tres,me especializo en podados de cesped y tengo 3 años podando casas");
        txtPrice = findViewById(R.id.service_detail_employee_txt_price);
        txtPrice.setText(String.format("$ %.2f",price));
        btnHire = findViewById(R.id.service_detail_employee_btn_hire);

    }

}
