package com.lustra.lustra.Sidebar;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lustra.lustra.R;

/**
 * Created by victorrosas on 10/9/17.
 */

public class DrawerItemCustomAdapter extends ArrayAdapter<DataModel> {

    Context mContext;
    int layoutResourceId;
    DataModel data[] = null;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, DataModel[] data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

        if(position == 0){//Este es para el header con la imagen
            listItem = inflater.inflate(R.layout.header_list,parent,false);
            TextView txtUsername = (TextView) listItem.findViewById(R.id.txt_header_username);

        }else{
            listItem = inflater.inflate(layoutResourceId, parent, false);
        }

        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);
        View selectedView = listItem.findViewById(R.id.sidebar_indicator);

        DataModel current = data[position];

        if(position != 0) {
            textViewName.setText(current.name);
        }

        return listItem;
    }


}
