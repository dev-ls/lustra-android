package com.lustra.lustra.Services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.lustra.lustra.Helpers.Objects.OBJ_Service;
import com.lustra.lustra.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victorrosas on 4/17/18.
 */

public class employeeEnlistedServicesFragment extends android.support.v4.app.Fragment {

    Button btnAddService;
    ListView lvServices;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_employee_enlisted_services,container,false);
        setup(v);
        return v;
    }

    public void setup(View v){

        btnAddService = v.findViewById(R.id.enlisted_services_btn_add_services);
        btnAddService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: Add Service
            }
        });

        List<OBJ_Service> services = new ArrayList<>();
        services.add(new OBJ_Service("Jardineria","Jardineria descripcion",getContext().getDrawable(R.drawable.gardening_)));

        lvServices = v.findViewById(R.id.lv_enlisted_services);
        adapterEnlistedServices laServices = new adapterEnlistedServices(getContext(),services);
        lvServices.setAdapter(laServices);

    }

}
