package com.lustra.lustra.Employee;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.lustra.lustra.R;

/**
 * Created by victorrosas on 4/10/18.
 */

public class employeeConditionsFragment extends android.support.v4.app.Fragment {

    RecyclerView lvEmployeeActivities;
    TextView txtEmployeeConditionsMin;
    TextView txtEmployeeConditionsMax;
    EditText txtStartHour;
    EditText txtEndHour;
    ListView lvWorkingHours;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_employee_service_conditions,container,false);
        setup(v);
        return v;
    }

    public void setup(View v){
        lvEmployeeActivities = v.findViewById(R.id.lv_employee_activities);
        txtEmployeeConditionsMin = v.findViewById(R.id.employee_conditions_txt_service_min);
        txtEmployeeConditionsMax = v.findViewById(R.id.employee_conditions_txt_service_max);
        txtStartHour = v.findViewById(R.id.employee_conditions_starthour);
        txtEndHour = v.findViewById(R.id.employee_conditions_endhour);
        lvWorkingHours = v.findViewById(R.id.employee_conditions_hours);

    }


}
