package com.lustra.lustra.Helpers;

import android.app.Activity;
import android.util.Log;

import com.lustra.lustra.Helpers.Objects.OBJ_Card;

import org.json.JSONObject;

import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;

/**
 * Created by victorrosas on 8/15/16.
 */
public class CardCreatorHelper {

    private OBJ_Card tarjeta;
    private Activity activity;
    private String publicKey;
    private String userid;


    public CardCreatorHelper(Activity activity){
        this.activity = activity;
    }

    public void Conekta_NewCard(OBJ_Card tarjeta){
        this.tarjeta = tarjeta;
        publicKey = "key_EjtxPsDy7e7yTy3M1CBu4ew";
        userid = "75";
    }


    public void Conekta_CreateCard(OBJ_Card tarjeta){

        Conekta.setPublicKey(publicKey);// LLamamos al API para obtener llave de conekta
        Conekta.collectDevice(activity);
        Card card = new Card(tarjeta.card_name,tarjeta.card_number,tarjeta.card_CCV,tarjeta.card_expmonth,tarjeta.card_expyear);
        Token token = new Token(activity);

        token.onCreateTokenListener(new Token.CreateToken() {
            @Override
            public void onCreateTokenReady(JSONObject data) {
                try {
                    //TODO: Create charge
                    Log.d("Token::::", data.getString("id"));
                    System.out.println(data);
                } catch (Exception err) {
                    //TODO: Handle error
                    //Log.d("Error: " + err.toString());
                }
            }
        });

        token.create(card);
        System.out.println("Card created" + token);
    }

}
