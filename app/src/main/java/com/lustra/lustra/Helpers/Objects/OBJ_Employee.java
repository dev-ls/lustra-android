package com.lustra.lustra.Helpers.Objects;

import android.graphics.Bitmap;

import org.joda.time.DateTime;

import java.util.Map;

/**
 * Created by victorrosas on 3/9/18.
 */

public class OBJ_Employee{

    Integer id;
    Bitmap imgPhoto;
    String name;
    String experience;

    //Days of the week as in [0 Sunday 1 Monday 2 Tuesday ...]
    //Getting the day of the week gets you an array with available hours (2+ to create a range)
    Map<Integer,DateTime[]> availability;

    OBJ_Review[] ratings;
    Double price;
    String specialty; //Working on a tag system
    String[] tags;

    public OBJ_Employee(Integer id, Bitmap imgPhoto, String name, String experience, Map<Integer, DateTime[]> availability, OBJ_Review[] ratings, Double price, String specialty, String[] tags) {
        this.id = id;
        this.imgPhoto = imgPhoto;
        this.name = name;
        this.experience = experience;
        this.availability = availability;
        this.ratings = ratings;
        this.price = price;
        this.specialty = specialty;
        this.tags = tags;
    }

    public Bitmap getImgPhoto() {
        return imgPhoto;
    }

    public String getName() {
        return name;
    }

    public String getExperience() {
        return experience;
    }

    public Map<Integer, DateTime[]> getAvailability() {
        return availability;
    }

    public OBJ_Review[] getRatings() {
        return ratings;
    }

    public Double getPrice() {
        return price;
    }

    public String getSpecialty() {
        return specialty;
    }

    public String[] getTags() {
        return tags;
    }

}
