package com.lustra.lustra.Inbox;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lustra.lustra.R;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victorrosas on 3/10/18.
 */

public class inboxFragment extends android.support.v4.app.Fragment {

    ListView lvInbox;
    adapterInbox laInbox;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inbox,container,false);
        setup(v);
        return v;
    }

    public void setup(View v){

        lvInbox = v.findViewById(R.id.list_inbox_messages);
        laInbox = new adapterInbox(getContext(),getMessages());
        lvInbox.setAdapter(laInbox);

    }

    public List<OBJ_Inboxmsg> getMessages(){

        List<OBJ_Inboxmsg> messages = new ArrayList<>();
        Bitmap profilepic =  ((BitmapDrawable) ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.person_2_profile_, null)).getBitmap();
        messages.add(new OBJ_Inboxmsg(1,profilepic,"Nombre","Mensaje Corto","Mensaje Completo",new DateTime(),"Status"));
        messages.add(new OBJ_Inboxmsg(1,profilepic,"Nombre","Mensaje Corto","Mensaje Completo",new DateTime(),"Status"));
        messages.add(new OBJ_Inboxmsg(1,profilepic,"Nombre","Mensaje Corto","Mensaje Completo",new DateTime(),"Status"));
        messages.add(new OBJ_Inboxmsg(1,profilepic,"Nombre","Mensaje Corto","Mensaje Completo",new DateTime(),"Status"));

        return messages;
    }


}
