package com.lustra.lustra.Helpers;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by victorrosas on 3/16/18.
 */

public class dateHelper {

    public static String getDaysFromIntegers(Set<Integer> days){

        /*String[] strDays = new String[] { " D ", " L ", " M "," X ", " J "," V ", " S " };
        String workingDays = "";

        for (Integer day:days) {
            workingDays += strDays[day];
        }

        return workingDays;*/

        return "L M X J V S D";
    }

    public static String getHoursFromDates(Map<Integer,DateTime[]> availability){
        //TODO: Ver la logica para manejar las horas
        return "09:00 - 14:00";
    }

    public static String DateTimetoString(DateTime dateTime){
        if(dateTime != null){
            return dateTime.toString();
        }

        return "dd-mm-yyyy";
    }

    public static Calendar dateToCalendar(Date date){
        return new DateTime(date).toCalendar(Locale.getDefault());
    }


}
