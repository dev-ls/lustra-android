package com.lustra.lustra.Services;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lustra.lustra.R;

/**
 * Created by victorrosas on 3/12/18.
 */

public class serviceActivity extends Activity {

    RelativeLayout titleContainer;
    TextView txtTitle;
    ImageView imgTitle;

    TextView txtWorkDescription;
    Button btnWorkRegister;
    Button btnWorkLogin;

    LinearLayout hireContainer;
    TextView txtSubtitle;
    TextView txtServiceDescription;
    Button btnClientRegister;
    Button btnClientLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_services_description);

        Integer serviceType = getIntent().getIntExtra("tagServiceId",0);

        setup(serviceType);
    }

    public void setup(Integer servicetype){

        titleContainer = findViewById(R.id.service_RL_title_container);
        txtTitle = findViewById(R.id.service_txt_title);
        imgTitle = findViewById(R.id.service_img_title);

        txtWorkDescription = findViewById(R.id.services_txt_work_description);

        btnWorkRegister = findViewById(R.id.services_btn_work_register);
        btnWorkRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnWorkLogin = findViewById(R.id.services_btn_work_login);
        btnWorkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        hireContainer = findViewById(R.id.services_LL_hire_container);
        txtSubtitle = findViewById(R.id.services_txt_subtitle);
        txtServiceDescription = findViewById(R.id.services_txt_service_description);
        btnClientLogin = findViewById(R.id.services_btn_client_login);
        btnClientLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btnClientRegister = findViewById(R.id.services_btn_client_register);
        btnClientRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        switch(servicetype){ // [ 0 Carwash,1 Gardening,2 Cleaning]
            case 0://Car wash is already the default
                break;
            case 1:
                titleContainer.setBackground(getDrawable(R.drawable.gardening_));
                txtTitle.setText(getText(R.string.title_gardening));
                imgTitle.setImageDrawable(getDrawable(R.drawable.sir_garden));
                txtSubtitle.setText(getText(R.string.subtitle_gardening));
                txtWorkDescription.setText(getText(R.string.work_gardening));
                hireContainer.setBackground(getDrawable(R.drawable.background_green_dark));
                txtServiceDescription.setText(getText(R.string.hire_gardening));
                break;
            case 2:
                titleContainer.setBackground(getDrawable(R.drawable.tiles_cleaning));
                txtTitle.setText(getText(R.string.title_cleaning));
                imgTitle.setImageDrawable(getDrawable(R.drawable.cleaning1));
                txtSubtitle.setText(getText(R.string.subtitle_cleaning));
                txtWorkDescription.setText(getText(R.string.work_cleaning));
                hireContainer.setBackground(getDrawable(R.drawable.background_sky_blue));
                txtServiceDescription.setText(getText(R.string.hire_cleaning));
                break;
            default:
                break;

        }
    }

}
