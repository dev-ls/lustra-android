package com.lustra.lustra.Helpers.Objects;

/**
 * Created by victorrosas on 8/15/16.
 */
public class OBJ_Card {

    public String card_Id;
    public String card_name;
    public String card_number;
    public String card_last4;
    public String card_expmonth;
    public String card_expyear;
    public String card_CCV;
    public Boolean card_isDefault;

    public OBJ_Card(String name, String number, String last4, String mes, String año, String ccv){
        this.card_Id = "";
        this.card_name = name;
        this.card_number = number;
        this.card_last4 = last4;
        this.card_expmonth = mes;
        this.card_expyear = año;
        this.card_CCV = ccv;
        this.card_isDefault = true;
    }

    public OBJ_Card(String id, String last4, String name, Boolean isdefault){
        this.card_Id = id;
        this.card_last4 = last4;
        this.card_name = name;
        this.card_isDefault = isdefault;
    }

}
