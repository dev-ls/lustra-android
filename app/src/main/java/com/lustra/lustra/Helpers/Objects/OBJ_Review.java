package com.lustra.lustra.Helpers.Objects;

import android.graphics.Bitmap;

/**
 * Created by victorrosas on 3/9/18.
 */

public class OBJ_Review {

    Integer reviewId;
    Bitmap reviewerImage;
    String reviewerName;
    Integer numStars;
    String reviewString;

    public OBJ_Review(Integer reviewId, Bitmap reviewerImage, String reviewerName, Integer numStars, String reviewString) {
        this.reviewId = reviewId;
        this.reviewerImage = reviewerImage;
        this.reviewerName = reviewerName;
        this.numStars = numStars;
        this.reviewString = reviewString;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public Bitmap getReviewerImage() {
        return reviewerImage;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public Integer getNumStars() {
        return numStars;
    }

    public String getReviewString() {
        return reviewString;
    }
}
