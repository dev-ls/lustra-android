package com.lustra.lustra.Settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lustra.lustra.Helpers.SessionHelper;
import com.lustra.lustra.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victorrosas on 3/10/18.
 */

public class settingsFragment extends Fragment {

    TextView txtName;
    ImageView imgProfile;
    ListView lvSettings;
    adapterTextImagePair laSettings;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings,container,false);
        setup(v);
        return v;
    }

    public void setup(View v){

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        String name = user.getDisplayName();

        txtName = v.findViewById(R.id.settings_txt_profile_name);
        txtName.setText(name);

        imgProfile = v.findViewById(R.id.settings_img_profile);
        //if(user.getPhotoUrl() != null){
            Picasso.get()
                    .load(user.getPhotoUrl())
                    .placeholder(R.drawable.person_2_profile_)
                    .error(R.drawable.person_2_profile_)
                    .into(imgProfile);
        /*}else{
            imgProfile.setImageBitmap(UserPicture.getRoundedShape(((BitmapDrawable) ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.person_2_profile_, null)).getBitmap()));
        }*/

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent("com.lustra.lustra.Profile.profileFragment");
               startActivity(intent);
            }
        });

        lvSettings = v.findViewById(R.id.lv_settings_options);
        lvSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){

                    case 4:
                        Intent intent = new Intent("com.lustra.Settings.paymentMethods.paymentMethodFragment");
                        startActivity(intent);

                    case 8:
                        SessionHelper.signOut(getContext());
                        break;


                }
            }
        });
        laSettings = new adapterTextImagePair(getContext(),getSettings());
        lvSettings.setAdapter(laSettings);

    }

    public List<OBJ_TextImagePair> getSettings(){
        List<OBJ_TextImagePair> settings = new ArrayList<>();
        settings.add(new OBJ_TextImagePair("Notificaciones",getContext().getDrawable(R.drawable.bell)));
        settings.add(new OBJ_TextImagePair("Configuraciones",getContext().getDrawable(R.drawable.gear_7)));
        settings.add(new OBJ_TextImagePair("Invita Amigos",getContext().getDrawable(R.drawable.persons_)));
        settings.add(new OBJ_TextImagePair("Referir a un Lustra ayudante",getContext().getDrawable(R.drawable.lustra_icon_black)));
        settings.add(new OBJ_TextImagePair("Pagos",getContext().getDrawable(R.drawable.credit_card_6_128x128)));
        settings.add(new OBJ_TextImagePair("Estadisticas",getContext().getDrawable(R.drawable.stats)));
        settings.add(new OBJ_TextImagePair("Ayuda y Feedback",getContext().getDrawable(R.drawable.rocket)));
        settings.add(new OBJ_TextImagePair("Vista Empleado",getContext().getDrawable(R.drawable.person_2_profile_)));
        settings.add(new OBJ_TextImagePair("Logout",null));


        return settings;
    }

}
