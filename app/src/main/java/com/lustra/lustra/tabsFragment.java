package com.lustra.lustra;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Calendar.calendarAgendaFragment;
import com.lustra.lustra.Inbox.inboxFragment;
import com.lustra.lustra.Search.searchFragment;
import com.lustra.lustra.Services.employeeEnlistedServicesFragment;
import com.lustra.lustra.Settings.settingsFragment;

/**
 * Created by victorrosas on 3/12/18.
 */

public class tabsFragment extends FragmentActivity {

    private FragmentTabHost tabHost;
    Integer selectedTab = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        selectedTab = getIntent().getIntExtra("def_tab", 0);

        setContentView(R.layout.fragment_tabs);
        tabHost = findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(getTabIndicator(tabHost.getContext(),R.string.empty_string, R.drawable.search_1, "", "")),
                searchFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(getTabIndicator(tabHost.getContext(),R.string.empty_string, R.drawable.calendar_interface_symbol_tool_318_58214, "", "")),
                calendarAgendaFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator(getTabIndicator(tabHost.getContext(),R.string.empty_string, R.drawable.lustra_icon_black, "", "")),
                employeeEnlistedServicesFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab4").setIndicator(getTabIndicator(tabHost.getContext(),R.string.empty_string, R.drawable.chat_35, "", "")),
                inboxFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab5").setIndicator(getTabIndicator(tabHost.getContext(),R.string.empty_string, R.drawable.person_2_profile_, "", "")),
                settingsFragment.class, null);

        /*if(selectedTab != 0){
            tabHost.setCurrentTab(selectedTab);
        }*/

    }

    private View getTabIndicator(Context context, int title, int icon, String leftbadge, String rightbadge) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView);
        iv.setImageResource(icon);
        TextView tv = (TextView) view.findViewById(R.id.textView);
        tv.setText(title);

        if(!leftbadge.isEmpty()) {
            TextView leftBadge = (TextView) view.findViewById(R.id.tab_left_badge);
            leftBadge.setBackground(getResources().getDrawable(R.drawable.badge_gray));
            leftBadge.setText(leftbadge);
        }
        if(!rightbadge.isEmpty()) {
            TextView rightBadge = (TextView) view.findViewById(R.id.tab_right_badge);
            rightBadge.setText(rightbadge);
            rightBadge.setBackground(getResources().getDrawable(R.drawable.badge_red));
        }
        return view;
    }


}
