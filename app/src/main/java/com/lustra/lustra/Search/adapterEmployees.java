package com.lustra.lustra.Search;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.UserPicture;
import com.lustra.lustra.Helpers.dateHelper;
import com.lustra.lustra.Helpers.textHelper;
import com.lustra.lustra.Helpers.Objects.OBJ_Employee;
import com.lustra.lustra.R;

import java.util.List;

/**
 * Created by victorrosas on 3/16/18.
 */

public class adapterEmployees extends ArrayAdapter<OBJ_Employee> {

    public adapterEmployees(Context context, List<OBJ_Employee> objects){
        super(context,0,objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Salvando la referencia del View de la fila
        View listItemView = convertView;

        //Comprobando si el View no existe
        if (null == convertView) {
            //Si no existe, entonces inflarlo con image_list_view.xml
            listItemView = inflater.inflate(
                    R.layout.cell_employees_available,
                    parent,
                    false);

        }

        TextView txtAvailability = listItemView.findViewById(R.id.employees_txt_availability);
        TextView txtHours = listItemView.findViewById(R.id.employees_txt_hours);
        TextView txtExperience = listItemView.findViewById(R.id.employees_txt_experience);
        TextView txtTags = listItemView.findViewById(R.id.employees_txt_tags);
        ImageView  imgProfile = listItemView.findViewById(R.id.employees_img_profile);
        ImageView imgRatingStars = listItemView.findViewById(R.id.employees_img_stars);
        TextView txtName = listItemView.findViewById(R.id.employees_txt_name);
        TextView txtSpecialty = listItemView.findViewById(R.id.employees_txt_specialty);
        TextView txtPrice = listItemView.findViewById(R.id.employees_txt_price);
        Button btnHire = listItemView.findViewById(R.id.btn_hire);

        final OBJ_Employee employee = getItem(position);

        txtAvailability.setText(dateHelper.getDaysFromIntegers(employee.getAvailability().keySet()));
        txtHours.setText(dateHelper.getHoursFromDates(employee.getAvailability()));
        txtExperience.setText(employee.getExperience());
        txtTags.setText(textHelper.getTags(employee.getTags()));

        Bitmap profilepic =  ((BitmapDrawable) ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.person_2_profile_, null)).getBitmap();
        imgProfile.setImageBitmap(UserPicture.getRoundedShape(profilepic));

        imgRatingStars.setImageBitmap(((BitmapDrawable) ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.starts, null)).getBitmap());//TODO: Poner logica de estrellas
        txtName.setText(employee.getName());
        txtSpecialty.setText("Especialidad: " + employee.getSpecialty());
        txtPrice.setText(String.format("$ %.2f",employee.getPrice()));

        btnHire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.lustra.lustra.Employee.serviceDetailFragment");
               // intent.putExtra("tagBitmap",employee.getImgPhoto());
                intent.putExtra("tagPrice",employee.getPrice());
                getContext().startActivity(intent);
            }
        });

        //Devolver al ListView la fila creada
        return listItemView;
    }

}
