package com.lustra.lustra.Settings;

import android.graphics.drawable.Drawable;

/**
 * Created by victorrosas on 3/17/18.
 */

public class OBJ_TextImagePair  {

    String textValue;
    Drawable img;

    public OBJ_TextImagePair(String text, Drawable img) {
        this.textValue = text;
        this.img = img;
    }

    public String getTextValue() {
        if(textValue != null) {
            return textValue;
        }
        return "";
    }

    public Drawable getImg() {
        return img;
    }
}
