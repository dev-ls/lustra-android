package com.lustra.lustra.Sidebar;

/**
 * Created by victorrosas on 10/9/17.
 */

public class DataModel {

    public int icon;
    public String name;
    public Boolean isSelected;

    // Constructor.
    public DataModel(int icon, String name,Boolean selected) {

        this.icon = icon;
        this.name = name;
        this.isSelected = selected;
    }

    //Getters
    public Boolean getSelected() {
        if(isSelected != null) {
            return isSelected;
        }
        return false;
    }

    //Setters
    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

}
