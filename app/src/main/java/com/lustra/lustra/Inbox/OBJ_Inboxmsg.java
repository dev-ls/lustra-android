package com.lustra.lustra.Inbox;

import android.graphics.Bitmap;

import org.joda.time.DateTime;

/**
 * Created by victorrosas on 3/10/18.
 */

public class OBJ_Inboxmsg {

    Integer id;
    Bitmap imgProfile;
    String name;
    String messageShort; // Show preview message
    String message;
    DateTime received; // Use this date to calculate received ago
    String status; // Service status [completed/canceled/pending]

    public OBJ_Inboxmsg(Integer id, Bitmap imgProfile, String name, String messageShort, String message, DateTime received, String status) {
        this.id = id;
        this.imgProfile = imgProfile;
        this.name = name;
        this.messageShort = messageShort;
        this.message = message;
        this.received = received;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public Bitmap getImgProfile() {
        return imgProfile;
    }

    public String getName() {
        return name;
    }

    public String getMessageShort() {
        return messageShort;
    }

    public String getMessage() {
        return message;
    }

    public DateTime getReceived() {
        return received;
    }

    public String getStatus() {
        return status;
    }
}
