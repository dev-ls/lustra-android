package com.lustra.lustra.Helpers;

import android.content.Context;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;

public class SessionHelper {

    public static void signOut(Context context){
        FirebaseAuth.getInstance().signOut();
        //Method of signOut
        Intent intent = new Intent("com.lustra.lustra.login");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void login(Context context){
        Intent intent = new Intent("com.lustra.lustra.tabsFragment");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
