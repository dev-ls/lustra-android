package com.lustra.lustra.Landing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.lustra.lustra.R;

/**
 * Created by victorrosas on 3/13/18.
 */

public class landingActivity extends Activity {

    Button btnLogin;
    Button btnRegister;

    RelativeLayout imgCarWash;
    RelativeLayout imgGardening;
    RelativeLayout imgCleaning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_screen);
        setup();
    }

    public void setup() {
        btnLogin = findViewById(R.id.landing_btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.lustra.lustra.login");
                startActivity(intent);
            }
        });

        btnRegister = findViewById(R.id.landing_btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.lustra.lustra.clientRegisterActivity");
                startActivity(intent);
            }
        });

        imgCarWash = findViewById(R.id.landing_RL_carwash);
        imgCarWash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.lustra.lustra.Services.serviceActivity");
                intent.putExtra("tagServiceId",0);
                startActivity(intent);
            }
        });

        imgGardening = findViewById(R.id.landing_RL_gardening);
        imgGardening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.lustra.lustra.Services.serviceActivity");
                intent.putExtra("tagServiceId",1);
                startActivity(intent);
            }
        });


        imgCleaning = findViewById(R.id.landing_RL_cleaning);
        imgCleaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.lustra.lustra.Services.serviceActivity");
                intent.putExtra("tagServiceId",2);
                startActivity(intent);
            }
        });


    }

}
