package com.lustra.lustra.Services;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lustra.lustra.Helpers.Objects.OBJ_Service;
import com.lustra.lustra.R;

import java.util.List;

/**
 * Created by victorrosas on 4/17/18.
 */

public class adapterEnlistedServices extends ArrayAdapter<OBJ_Service> {

    public adapterEnlistedServices(Context context, List<OBJ_Service> objects){
        super(context,0,objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Salvando la referencia del View de la fila
        View listItemView = convertView;

        //Comprobando si el View no existe
        if (null == convertView) {
            //Si no existe, entonces inflarlo con image_list_view.xml
            listItemView = inflater.inflate(
                    R.layout.cell_enlisted_services,
                    parent,
                    false);

        }

        TextView txtCategory = listItemView.findViewById(R.id.cell_enlisted_services_txt_service_name);
        TextView txtDescription = listItemView.findViewById(R.id.cell_enlisted_services_txt_service_description);
        ImageView imgService = listItemView.findViewById(R.id.cell_enlisted_services_service_img);

        OBJ_Service service = getItem(position);

        txtCategory.setText(service.getServiceCategory());
        txtDescription.setText(service.getServiceDescription());
        imgService.setImageDrawable(service.getServiceImage());

        return listItemView;
    }

}
